# Tycho 2 Parser

This project aims to provide a library for parsing Tycho 2 catalog data.

## Data Sources

The first iteration (tag v0.1.0) uses data from [this](http://archive.eso.org/ASTROM/TYC-2/data/) Tycho-2 catalogue, and uses [this](http://tdc-www.harvard.edu/catalogs/tycho2.format.html) format definition for parsing.

## Installation and Usage

1. Clone this repository:
   1. ```bash
      git clone https://gitlab.com/peter-kingsbury/tycho-2-parser.git
      ``` 
2. Install dependencies:
   1. ```bash
      npm install
      ```
3. Obtain the Tycho-2 catalogue:
   1. ```bash
      curl -o data/catalog.dat http://archive.eso.org/ASTROM/TYC-2/data/catalog.dat
      curl -o data/index.dat   http://archive.eso.org/ASTROM/TYC-2/data/index.dat
      curl -o data/suppl_1.dat http://archive.eso.org/ASTROM/TYC-2/data/suppl_1.dat
      curl -o data/suppl_2.dat http://archive.eso.org/ASTROM/TYC-2/data/suppl_2.dat
      ```
4. Run the parser:
   1. ```bash
      npm start
      ```

## Design Considerations

Due to the sheer volume of record data (the `catalog.dat` data contains 2,539,913 records), I opted to implement the main transformation class as an extension to Node.js Stream Transform ([see docs](https://nodejs.org/api/stream.html#class-streamtransform)) so that it can be fit into a data transformation pipeline. 

The record parsing itself is rather rudimentary; it would be better as a regular expression so that record-matching could be more effectively done per record type as found in each data-file.
