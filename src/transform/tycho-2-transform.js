const { Transform } = require('stream');
const {
  parseIntOrZero,
  parseIntOrNull,
  parseFloatOrZero,
  trimStringOrNull
} = require('../util/parse-data');

const DATA_SOURCE = 'tycho2';

class Tycho2Transform extends Transform {
  constructor(options) {
    super(options);

    this.currentLine = '';
    this.lineLength = null;
    this.parsedLines = null;
  }

  getSource() {
    return DATA_SOURCE;
  }

  _transform(data, encoding, callback) {
    const line = data.toString();

    this.parsedLines = '';
    this.currentLine += line;

    this.currentLine.split(/\r?\n/).forEach(line => {
      if (!this.lineLength) {
        this.lineLength = line.length;
      }

      if (line.length < this.lineLength) {
        // This must be the end of a chunk; store it for the next chunk
        this.currentLine = line;
      } else {
        const fields = line.split('|');
        const [
          tyc,
          pflag,
          mRAdeg,
          mDEdeg,
          pmRA,
          pmDE,
          e_mRA,
          e_mDE,
          e_pmRA,
          e_pmDE,
          mepRA,
          mepDE,
          Num,
          g_mRA,
          g_mDE,
          g_pmRA,
          g_pmDE,
          BT,
          e_BT,
          VT,
          e_VT,
          prox,
          TYC,
          HIP,
          //CCDM,
          RAdeg,
          DEdeg,
          epRA,
          epDE,
          e_RA,
          e_DE,
          posflg,
          corr
        ] = fields;
        const [TYC1, TYC2, TYC3] = tyc.split(' ');

        const record = Object.assign(
          {},
          {
            source: this.getSource(),
            TYC1: parseIntOrZero(TYC1),
            TYC2: parseIntOrZero(TYC2),
            TYC3: parseIntOrZero(TYC3),
            pflag: trimStringOrNull(pflag),
            mRAdeg: parseFloatOrZero(mRAdeg),
            mDEdeg: parseFloatOrZero(mDEdeg),
            pmRA: parseFloatOrZero(pmRA),
            pmDE: parseFloatOrZero(pmDE),
            e_mRA: parseIntOrZero(e_mRA),
            e_mDE: parseIntOrZero(e_mDE),
            e_pmRA: parseFloatOrZero(e_pmRA),
            e_pmDE: parseFloatOrZero(e_pmDE),
            mepRA: parseFloatOrZero(mepRA),
            mepDE: parseFloatOrZero(mepDE),
            Num: parseIntOrZero(Num),
            g_mRA: parseFloatOrZero(g_mRA),
            g_mDE: parseFloatOrZero(g_mDE),
            g_pmRA: parseFloatOrZero(g_pmRA),
            g_pmDE: parseFloatOrZero(g_pmDE),
            BT: parseFloatOrZero(BT),
            e_BT: parseFloatOrZero(e_BT),
            VT: parseFloatOrZero(VT),
            e_VT: parseFloatOrZero(e_VT),
            prox: parseIntOrZero(prox),
            TYC: parseIntOrNull(TYC),
            HIP: parseIntOrZero(HIP),
            /*CCDM,*/
            RAdeg: parseFloatOrZero(RAdeg),
            DEdeg: parseFloatOrZero(DEdeg),
            epRA: parseFloatOrZero(epRA),
            epDE: parseFloatOrZero(epDE),
            e_RA: parseFloatOrZero(e_RA),
            e_DE: parseFloatOrZero(e_DE),
            posflg: trimStringOrNull(posflg),
            corr: parseFloatOrZero(corr)
          }
        );

        this.parsedLines += `${JSON.stringify(record)}\n`;
      }
    });

    callback(null, this.parsedLines);
  }
}

module.exports = { Tycho2Transform };
