const fs = require('fs');
const path = require('path');
const { pipeline } = require('stream');
const { Tycho2Transform } = require('./transform/tycho-2-transform');

const outfile = path.join(process.cwd(), 'output', 'merged-catalogue.ndjson');

/**
 * Data transformation pipeline.
 * 1. Read input data from the infile stream.
 * 2. Parse and transform the data into a stringified JavaScript Object.
 * 3. Record the data in a .ndjson file.
 * @param {String} infile
 * @returns {Promise<unknown>}
 */
async function transformTycho2DataFile(infile) {
  return new Promise((resolve, reject) => {
    const tycho2Transformer = new Tycho2Transform();
    const inputStream = fs.createReadStream(infile);
    const outputStream = fs.createWriteStream(outfile);
    pipeline(inputStream, tycho2Transformer, outputStream, err => {
      if (err) {
        return reject(err);
      }

      resolve();
    });
  });
}

/**
 *
 * @param dir
 * @param ext
 * @returns {string[]}
 */
function listFilesInDir(dir, ext) {
  return fs.readdirSync(dir).filter(fn => fn.endsWith(ext));
}

(async () => {
  const p = [];
  const dataDir = path.join(process.cwd(), 'data');
  const files = listFilesInDir(dataDir, 'dat');

  for (const filename of files) {
    const infile = path.join(process.cwd(), 'data', filename);
    p.push(transformTycho2DataFile(infile));
  }

  await Promise.all(p);
})();
