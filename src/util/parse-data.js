function parseIntOrZero(data) {
  return isNaN(parseInt(data)) ? 0 : parseInt(data);
}

function parseIntOrNull(data) {
  return isNaN(parseInt(data)) ? null : parseInt(data);
}

function parseFloatOrZero(data) {
  return isNaN(parseFloat(data)) ? 0.0 : parseFloat(data);
}

function parseFloatOrNull(data) {
  return isNaN(parseFloat(data)) ? null : parseFloat(data);
}

function trimStringOrNull(data) {
  return data.trim().length === 0 ? null : data.trim();
}

module.exports = {
  parseIntOrZero,
  parseIntOrNull,
  parseFloatOrZero,
  parseFloatOrNull,
  trimStringOrNull
};
